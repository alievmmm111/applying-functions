-- Creating the view
CREATE OR REPLACE VIEW sales_revenue_by_category_qtr AS
SELECT 
    film_category, 
    SUM(sales_revenue) AS total_sales_revenue
FROM 
    sales_table
WHERE 
    DATE_PART('quarter', sales_date) = DATE_PART('quarter', CURRENT_DATE)
GROUP BY 
    film_category
HAVING 
    SUM(sales_revenue) > 0;

-- Creating the function
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter INTEGER)
RETURNS TABLE (film_category VARCHAR, total_sales_revenue NUMERIC) AS $$
BEGIN
    RETURN QUERY 
    SELECT 
        film_category, 
        SUM(sales_revenue) AS total_sales_revenue
    FROM 
        sales_table
    WHERE 
        DATE_PART('quarter', sales_date) = current_quarter
    GROUP BY 
        film_category
    HAVING 
        SUM(sales_revenue) > 0;
END; $$
LANGUAGE plpgsql;

-- Drop the function if it exists before recreating it
DO $$ 
BEGIN
    IF EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_name = 'new_movie') THEN
        DROP FUNCTION new_movie(VARCHAR);
    END IF;
END $$;

-- Creating the procedure
CREATE OR REPLACE PROCEDURE new_movie(movie_title VARCHAR)
LANGUAGE plpgsql
AS $$
DECLARE 
    language_id INTEGER;
BEGIN
    -- Check if the language exists
    SELECT id INTO language_id FROM language_table WHERE language = 'Klingon';
    IF language_id IS NULL THEN
        RAISE EXCEPTION 'Language does not exist in the language table.';
    END IF;

    -- Insert the new movie
    INSERT INTO film_table (title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), language_id);
    
    -- No need for COMMIT as CREATE and ALTER statements automatically commit
END; $$
